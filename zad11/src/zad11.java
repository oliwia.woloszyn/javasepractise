public class zad11 {

        public static void printEqual(int numA, int numB, int numC) {
            if (numA < 0 || numB < 0 || numC < 0) {
                System.out.println("Invalid Value");
            }
            if (numA == numB && numB == numC) {
                System.out.println("All numbers are equal");
            }
            if (numA != numB && numB != numC && numA!= numC) {
                System.out.println("All numbers are different");
            }
            if (!(numA == numB && numB == numC) && !(numA != numB && numB != numC && numA != numC)) {
                System.out.println("Neither all are equal or different");
            }
        }

        public static void main(String[] args) {
            printEqual(1, 1, 2);
            printEqual(1, 1, 1);
            printEqual(-1, -1, -1);
            printEqual(1, 2, 3);
        }
    }
