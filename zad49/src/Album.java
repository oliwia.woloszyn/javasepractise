import Song.Song;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Album {
    private String albumName;
    private String artistName;
    private SongList songs;

    public Album(String albumName, String artistName) {
        this.albumName = albumName;
        this.artistName = artistName;
        this.songs = new SongList();
    }

    public boolean addSong(String title, double duration) {
        return songs.addSong(title, duration);
    }

    public boolean addToPlaylist(int trackNumber, LinkedList<Song> playlist) {
        Song song = songs.findSong(trackNumber);
        if (song != null) {
            playlist.add(song);
            return true;
        } else {
            System.out.println("This album does not have a track " + trackNumber);
            return false;
        }
    }

    public boolean addToPlaylist(String title, LinkedList<Song> playlist) {
        Song song = songs.findSong(title);
        if (song != null) {
            playlist.add(song);
            return true;
        } else {
            System.out.println("The song " + title + " is not in this album");
            return false;
        }
    }

    public static class SongList {
        private List<Song> songs;

        public SongList() {
            this.songs = new ArrayList<>();
        }

        public boolean addSong(String title, double duration) {
            Song song = findSong(title);
            if (song == null) {
                songs.add(new Song(title, duration));
                return true;
            }
            return false;
        }

        public Song findSong(String title) {
            for (Song song : songs) {
                if (song.getTitle().equals(title)) {
                    return song;
                }
            }
            return null;
        }

        public Song findSong(int trackNumber) {
            if (trackNumber >= 1 && trackNumber <= songs.size()) {
                return songs.get(trackNumber - 1);
            }
            return null;
        }
    }
}