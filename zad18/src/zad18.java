public class zad18 {
    public static int sumEvenDigits(int number) {
        if (number < 0) {
            return -1;
        }

        int sum = 0;
        while (number > 0) {
            int lastDigit = number % 10;
            if (isEven(lastDigit)) {
                sum += lastDigit;
            }
            number /= 10;
        }
        return sum;
    }

    public static boolean isEven(int digit) {
        return digit % 2 == 0;
    }
    public static void main(String[] args) {
        System.out.println(sumEvenDigits(-22));
        System.out.println(sumEvenDigits(252));
        System.out.println(sumEvenDigits(123456789));

    }
}