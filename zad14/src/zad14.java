public class zad14 {
    public static void main(String[] args) {
        int[] yearsToCheck = {-1600, 1600, 2017, 2000};
        for (int year : yearsToCheck) {
            System.out.println(year + " is a leap year: " + isLeapYear(year));
        }

        int[][] monthsAndYears = {{1, 2020}, {2, 2020}, {2, 2018}, {-1, 2020}, {1, -2020}};
        for (int[] data : monthsAndYears) {
            int month = data[0];
            int year = data[1];
            int days = getDaysInMonth(month, year);
            if (days == -1) {
                System.out.println("Invalid input for month " + month + " and year " + year);
            } else {
                System.out.println("Month " + month + " in year " + year + " has " + days + " days.");
            }
        }
    }

    public static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) return false;
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }

    public static int getDaysInMonth(int month, int year) {
        if (month < 1 || month > 12 || year < 1 || year > 9999) return -1;

        return switch (month) {
            case 2 -> isLeapYear(year) ? 29 : 28;
            case 4, 6, 9, 11 -> 30;
            default -> 31;
        };
    }
}