public class zad9 {
    public static double area(double Radius) {
        if (Radius < 0) {
            return -1.0;
        } else {
            return Math.PI * Radius * Radius;
        }
    }

    public static double area(double a, double b) {
        if (a < 0 || b < 0) {
            return -1.0;
        } else {
            return a * b;
        }
    }

    public static void main(String[] args) {
        System.out.println(area(5.0));
        System.out.println(area(-1.0));
        System.out.println(area(5.0, 4.0));
        System.out.println(area(-1.0, 4.0));
    }
}

