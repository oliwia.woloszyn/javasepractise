import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
public class zad45 {


        private String title;
        private double duration;

        public zad45 (String title, double duration) {
            this.title = title;
            this.duration = duration;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return title + ": " + duration;
        }
    }

    class AlbumInfo {
        private String name;
        private String artist;
        private ArrayList<zad45 > songs;

        public AlbumInfo(String name, String artist) {
            this.name = name;
            this.artist = artist;
            this.songs = new ArrayList<>();
        }

        public boolean addSong(String title, double duration) {
            if (findSong(title) == null) {
                songs.add(new zad45 (title, duration));
                return true;
            }
            return false;
        }

        public zad45  findSong(String title) {
            for (zad45  song : songs) {
                if (song.getTitle().equals(title)) {
                    return song;
                }
            }
            return null;
        }

        public boolean addToPlaylist(String title, LinkedList<zad45 > playlist) {
            zad45  song = findSong(title);
            if (song != null) {
                playlist.add(song);
                return true;
            }
            return false;
        }

        public boolean addToPlaylist(int trackNumber, LinkedList<zad45 > playlist) {
            int index = trackNumber - 1;
            if (index >= 0 && index < songs.size()) {
                playlist.add(songs.get(index));
                return true;
            }
            return false;
        }
    }

