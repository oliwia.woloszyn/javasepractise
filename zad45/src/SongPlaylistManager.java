import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class SongPlaylistManager {
    public static void main(String[] args) {
        ArrayList<AlbumInfo> albums = new ArrayList<>();

        AlbumInfo album1 = new AlbumInfo("Stormbringer", "Deep Purple");
        album1.addSong("Stormbringer", 4.6);
        album1.addSong("Love don't mean a thing", 4.22);
        album1.addSong("Holy man", 4.3);
        album1.addSong("Hold on", 5.6);
        album1.addSong("Lady double dealer", 3.21);
        album1.addSong("You can't do it right", 6.23);
        album1.addSong("High ball shooter", 4.27);
        album1.addSong("The gypsy", 4.2);
        album1.addSong("Soldier of fortune", 3.13);
        albums.add(album1);

        AlbumInfo album2 = new AlbumInfo("For those about to rock", "AC/DC");
        album2.addSong("For those about to rock", 5.44);
        album2.addSong("I put the finger on you", 3.25);
        album2.addSong("Lets go", 3.45);
        album2.addSong("Inject the venom", 3.33);
        album2.addSong("Snowballed", 4.51);
        album2.addSong("Evil walks", 3.45);
        album2.addSong("C.O.D.", 5.25);
        album2.addSong("Breaking the rules", 5.32);
        album2.addSong("Night of the long knives", 5.12);
        albums.add(album2);

        LinkedList<zad45> playlist = new LinkedList<>();
        albums.get(0).addToPlaylist("You can't do it right", playlist);
        albums.get(0).addToPlaylist("Holy man", playlist);
        albums.get(0).addToPlaylist("Speed king", playlist);
        albums.get(0).addToPlaylist(9, playlist);
        albums.get(1).addToPlaylist(3, playlist);
        albums.get(1).addToPlaylist(2, playlist);
        albums.get(1).addToPlaylist(24, playlist);

        play(playlist);
    }

    public static void play(LinkedList<zad45> playlist) {
        ListIterator<zad45> iterator = playlist.listIterator();
        if (playlist.isEmpty()) {
            System.out.println("Playlist is empty.");
            return;
        }

        System.out.println("Now playing: " + iterator.next().toString());
        boolean forward = true;
        boolean quit = false;

        while (!quit) {
            int choice = getUserChoice();

            switch (choice) {
                case 0:
                    System.out.println("Playlist complete.");
                    quit = true;
                    break;
                case 1:
                    if (!forward) {
                        if (iterator.hasNext()) {
                            iterator.next();
                        }
                        forward = true;
                    }
                    if (iterator.hasNext()) {
                        System.out.println("Now playing: " + iterator.next().toString());
                    } else {
                        System.out.println("Reached the end of the playlist.");
                        forward = false;
                    }
                    break;
                case 2:
                    if (forward) {
                        if (iterator.hasPrevious()) {
                            iterator.previous();
                        }
                        forward = false;
                    }
                    if (iterator.hasPrevious()) {
                        System.out.println("Now playing: " + iterator.previous().toString());
                    } else {
                        System.out.println("We are at the start of the playlist.");
                        forward = true;
                    }
                    break;
                case 3:
                    if (forward) {
                        if (iterator.hasPrevious()) {
                            System.out.println("Replaying: " + iterator.previous().toString());
                            forward = false;
                        } else {
                            System.out.println("We are at the start of the playlist.");
                        }
                    } else {
                        if (iterator.hasNext()) {
                            System.out.println("Replaying: " + iterator.next().toString());
                            forward = true;
                        } else {
                            System.out.println("Reached the end of the playlist.");
                        }
                    }
                    break;
                case 4:
                    listSongs(playlist);
                    break;
            }
        }
    }

    public static int getUserChoice() {

        return 0;
    }

    public static void listSongs(LinkedList<zad45> playlist) {
        System.out.println("Songs in the playlist:");
        for (int i = 0; i < playlist.size(); i++) {
            System.out.println((i + 1) + ". " + playlist.get(i).toString());
        }
    }
}
