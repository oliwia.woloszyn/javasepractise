public class zad31 {

        private double width;
        private double height;

        public zad31() {
            this.width = 0.0;
            this.height = 0.0;
        }

        public zad31(double width, double height) {
            setWidth(width);
            setHeight(height);
        }

        public void setWidth(double width) {
            this.width = (width < 0) ? 0 : width;
        }

        public double getWidth() {
            return width;
        }

        public void setHeight(double height) {
            this.height = (height < 0) ? 0 : height;
        }

        public double getHeight() {
            return height;
        }

        public double getArea() {
            return width * height;
        }

        public static class Main {
            public static void main(String[] args) {
                zad31 wall = new zad31(5, 4);
                System.out.println("Area= " + wall.getArea());

                wall.setHeight(-1.5);
                System.out.println("Width= " + wall.getWidth());
                System.out.println("Height= " + wall.getHeight());
                System.out.println("Area= " + wall.getArea());
            }
        }
    }
