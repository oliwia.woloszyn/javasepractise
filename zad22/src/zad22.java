public class zad22 {
        public static int getGreatestCommonDivisor(int first, int second) {
            if (first < 10 || second < 10) {
                return -1;
            }

            while (first != second) {
                if (first > second) {
                    first = first - second;
                } else {
                    second = second - first;
                }
            }

            return first;
        }

        public static void main(String[] args) {
            System.out.println(getGreatestCommonDivisor(25, 15));
            System.out.println(getGreatestCommonDivisor(12, 30));
            System.out.println(getGreatestCommonDivisor(9, 18));
            System.out.println(getGreatestCommonDivisor(81, 153));


            if (getGreatestCommonDivisor(35, 14) == 7) {
                System.out.println("7");
            } else {
                System.out.println("Not found.");
            }

            boolean isPrime = false;
            int number = 17;
            for (int i = 2; i <= Math.sqrt(number); i++) {
                if (number % i == 0) {
                    isPrime = false;
                    break;
                } else {
                    isPrime = true;
                }
            }
            if (isPrime) {
                System.out.println(number + " prime number.");
            } else {
                System.out.println(number + " not a prime number.");
            }
        }
    }
