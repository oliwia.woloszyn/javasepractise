public class zad25 {

    public static boolean canPack(int bigCount, int smallCount, int goal) {
        if (bigCount < 0 || smallCount < 0 || goal < 0) {
            return false;
        }
        int bigKilos = bigCount * 5;
        if (bigKilos + smallCount < goal) {
            return false;
        }

        if (goal >= 5) {
            int bigBagsNeeded = goal / 5;

            if (bigBagsNeeded <= bigCount) {
                goal -= bigBagsNeeded * 5;
            } else {
                goal -= bigCount * 5;
            }
        }

        if (goal > 0) {
            return smallCount >= goal;
        }

        return true;
    }
    public static void main(String[] args) {
        System.out.println(canPack(1, 0, 4));
        System.out.println(canPack(1, 0, 5));
        System.out.println(canPack(0, 5, 4));
        System.out.println(canPack(2, 2, 11));
        System.out.println(canPack(-3, 2, 12));
        System.out.println(canPack(2, 2, 12));
        System.out.println(canPack(2, 0, 10));
        System.out.println(canPack(0, 0, 0));
    }
}