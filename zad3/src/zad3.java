import java.io.*;
public class zad3 {
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(394298);
        printMegaBytesAndKiloBytes(0);
        printMegaBytesAndKiloBytes(-55);
    }

    public static void printMegaBytesAndKiloBytes(int kBs) {
        String printMBKB;

        if (kBs < 0) {
            printMBKB = "invalid value";
        } else {
            int yy = kBs / 1024;
            int zz = kBs % 1024;

            printMBKB = kBs + " KB = " + yy + " MB i " + zz + " KB";
        }

        System.out.println(printMBKB);
    }
}