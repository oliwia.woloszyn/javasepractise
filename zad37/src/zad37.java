class Zad37 {

    static class Lamp {
        private final String style;
        private final boolean battery;
        private final int globRating;

        public Lamp(String style, boolean battery, int globRating) {
            this.style = style;
            this.battery = battery;
            this.globRating = globRating;
        }

        public void turnOn() {
            System.out.println("Lamp -> Turning on");
        }

        public String getStyle() {
            return style;
        }

        public boolean isBattery() {
            return battery;
        }

        public int getGlobRating() {
            return globRating;
        }
    }

    static class Bed {
        private final String style;
        private final int pillows;
        private final int height;
        private final int sheets;
        private final int quilt;

        public Bed(String style, int pillows, int height, int sheets, int quilt) {
            this.style = style;
            this.pillows = pillows;
            this.height = height;
            this.sheets = sheets;
            this.quilt = quilt;
        }

        public void make() {
            System.out.println("Bed -> Making | The bed is being made.");
        }

        public String getStyle() {
            return style;
        }

        public int getPillows() {
            return pillows;
        }

        public int getHeight() {
            return height;
        }

        public int getSheets() {
            return sheets;
        }

        public int getQuilt() {
            return quilt;
        }
    }

    static class Ceiling {
        private final int height;
        private final String paintedColor;

        public Ceiling(int height, String paintedColor) {
            this.height = height;
            this.paintedColor = paintedColor;
        }

        public int getHeight() {
            return height;
        }

        public String getPaintedColor() {
            return paintedColor;
        }
    }

    static class Wall {
        private final String direction;

        public Wall(String direction) {
            this.direction = direction;
        }

        public String getDirection() {
            return direction;
        }
    }

    static class Bedroom {
        private final Bed bed;
        private final Lamp lamp;

        public Bedroom(String name, Wall wall1, Wall wall2, Wall wall3, Wall wall4, Ceiling ceiling, Bed bed, Lamp lamp) {
            this.bed = bed;
            this.lamp = lamp;
        }

        public void makeBed() {
            System.out.print("Bedroom -> Making bed | ");
            bed.make();
        }

        public Lamp getLamp() {
            return lamp;
        }
    }

    public class Main {
        public static void main(String[] args) {
            Wall wall1 = new Wall("West");
            Wall wall2 = new Wall("East");
            Wall wall3 = new Wall("South");
            Wall wall4 = new Wall("North");
            Ceiling ceiling = new Ceiling(12, "Blue");
            Bed bed = new Bed("Modern", 4, 3, 2, 1);
            Lamp lamp = new Lamp("Classic", false, 75);
            Bedroom bedroom = new Bedroom("John's Bedroom", wall1, wall2, wall3, wall4, ceiling, bed, lamp);

            bedroom.makeBed();
            lamp.turnOn();
        }
    }
}