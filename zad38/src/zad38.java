public class zad38 {

    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;

    public zad38(int tonerLevel, boolean duplex) {
        if (tonerLevel >= 0 && tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = -1;
        }
        this.pagesPrinted = 0;
        this.duplex = duplex;
    }

    public int addToner(int tonerAmount) {
        if (tonerAmount > 0 && tonerAmount <= 100) {
            if (this.tonerLevel + tonerAmount <= 100) {
                this.tonerLevel += tonerAmount;
                return this.tonerLevel;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public int printPages(int pages) {
        int pagesToPrint = pages;
        if (this.duplex) {
            pagesToPrint = (pages + 1) / 2;
            System.out.println("Printing in duplex mode");
        }
        this.pagesPrinted += pagesToPrint;
        return pagesToPrint;
    }

    public int getPagesPrinted() {
        return pagesPrinted;
    }

    public static void main(String[] args) {
        zad38 printer = new zad38(50, true);
        System.out.println(printer.addToner(50));
        System.out.println("Number of pages printed: " + printer.getPagesPrinted());
        int pagesPrinted = printer.printPages(4);
        System.out.println("Number of pages printed: " + pagesPrinted);
        System.out.println("New total print count for printer: " + printer.getPagesPrinted());
        pagesPrinted = printer.printPages(2);
        System.out.println("Number of pages printed: " + pagesPrinted);
        System.out.println("New total print count for printer: " + printer.getPagesPrinted());
    }
}
