import java.util.Scanner;
import java.util.Arrays;
public class zad41 {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Enter the number of elements: ");
        int size = inputScanner.nextInt();
        int[] numbers = getNumbersArray(size);
        System.out.println("Entered numbers:");
        printIntArray(numbers);
        int[] descendingNumbers = sortDescending(numbers);
        System.out.println("\nSorted numbers in descending order:");
        printIntArray(descendingNumbers);
    }

    public static int[] getNumbersArray(int size) {
        Scanner inputScanner = new Scanner(System.in);
        int[] numbers = new int[size];
        System.out.println("Enter " + size + " integers:");
        for (int i = 0; i < size; i++) {
            numbers[i] = inputScanner.nextInt();
        }
        return numbers;
    }
    public static void printIntArray(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Number at index " + i + ": " + numbers[i]);
        }
    }
    public static int[] sortDescending(int[] numbers) {
        int[] sortedNumbers = Arrays.copyOf(numbers, numbers.length);
        Arrays.sort(sortedNumbers);
        for (int i = 0; i < sortedNumbers.length / 2; i++) {
            int temp = sortedNumbers[i];
            sortedNumbers[i] = sortedNumbers[sortedNumbers.length - 1 - i];
            sortedNumbers[sortedNumbers.length - 1 - i] = temp;
        }
        return sortedNumbers;
    }
}