public class zad35 {
    private double dimension;

    public zad35 (double dimension) {
        if (dimension < 0) {
            this.dimension = 0;
        } else {
            this.dimension = dimension;
        }
    }

    public double getDimension() {
        return dimension;
    }

    public double calculateArea() {
        return Math.PI * dimension * dimension;
    }
}

class CylindricalShape extends zad35  {
    private double length;

    public CylindricalShape(double radius, double length) {
        super(radius);

        if (length < 0) {
            this.length = 0;
        } else {
            this.length = length;
        }
    }

    public double getLength() {
        return length;
    }

    public double calculateVolume() {
        return calculateArea() * length;
    }
}

