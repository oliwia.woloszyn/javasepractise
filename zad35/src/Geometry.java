public class Geometry {
    public static void main(String[] args) {
        zad35 shape1 = new zad35(3.75);
        System.out.println("shape1.dimension= " + shape1.getDimension());
        System.out.println("shape1.area= " + shape1.calculateArea());
        CylindricalShape shape2 = new CylindricalShape(5.55, 7.25);
        System.out.println("shape2.dimension= " + shape2.getDimension());
        System.out.println("shape2.length= " + shape2.getLength());
        System.out.println("shape2.area= " + shape2.calculateArea());
        System.out.println("shape2.volume= " + shape2.calculateVolume());
    }
}
