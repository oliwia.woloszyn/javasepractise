public class zad39 {

        private String type;
        private String brand;

        public zad39(String type, String brand) {
            this.type = type;
            this.brand = brand;
        }

        public String startEngine() {
            return "Vehicle -> startEngine()";
        }

        public String accelerate() {
            return "Vehicle -> accelerate()";
        }

        public String brake() {
            return "Vehicle -> brake()";
        }

        public String getType() {
            return type;
        }

        public String getBrand() {
            return brand;
        }
    }

    class Car extends zad39 {
        private int doors;

        public Car(String brand, int doors) {
            super("Car", brand);
            this.doors = doors;
        }

        @Override
        public String startEngine() {
            return "Car with " + doors + " doors -> startEngine()";
        }

        @Override
        public String accelerate() {
            return "Car with " + doors + " doors -> accelerate()";
        }

        @Override
        public String brake() {
            return "Car with " + doors + " doors -> brake()";
        }
    }

    class Motorcycle extends zad39 {
        private String bikeType;

        public Motorcycle(String brand, String bikeType) {
            super("Motorcycle", brand);
            this.bikeType = bikeType;
        }

        @Override
        public String startEngine() {
            return bikeType + " Motorcycle -> startEngine()";
        }

        @Override
        public String accelerate() {
            return bikeType + " Motorcycle -> accelerate()";
        }

        @Override
        public String brake() {
            return bikeType + " Motorcycle -> brake()";
        }
    }

    class Truck extends zad39 {
        private int capacity;

        public Truck(String brand, int capacity) {
            super("Truck", brand);
            this.capacity = capacity;
        }

        @Override
        public String startEngine() {
            return "Truck with a capacity of " + capacity + " tons -> startEngine()";
        }

        @Override
        public String accelerate() {
            return "Truck with a capacity of " + capacity + " tons -> accelerate()";
        }

        @Override
        public String brake() {
            return "Truck with a capacity of " + capacity + " tons -> brake()";
        }
    }

