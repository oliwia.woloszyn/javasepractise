public class Main {
    public static void main(String[] args) {
        Car car = new Car("Ford", 4);
        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());

        Motorcycle motorcycle = new Motorcycle("Harley-Davidson", "Sport");
        System.out.println(motorcycle.startEngine());
        System.out.println(motorcycle.accelerate());
        System.out.println(motorcycle.brake());

        Truck truck = new Truck("Volvo", 20);
        System.out.println(truck.startEngine());
        System.out.println(truck.accelerate());
        System.out.println(truck.brake());
    }
}
