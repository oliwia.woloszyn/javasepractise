public class zad28 {
    public static boolean canPack(double width, double height, double areaPerBucket, int extraBuckets) {
        if (width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBuckets < 0) {
            return false;
        }

        double wallArea = width * height;
        int requiredBuckets = (int) Math.ceil(wallArea / areaPerBucket);

        return requiredBuckets <= extraBuckets;
    }

    public static boolean canPack(double width, double height, double areaPerBucket) {
        if (width <= 0 || height <= 0 || areaPerBucket <= 0) {
            return false;
        }

        double wallArea = width * height;
        return wallArea <= areaPerBucket;
    }

    public static boolean canPack(double area, double areaPerBucket) {
        if (area <= 0 || areaPerBucket <= 0) {
            return false;
        }
        return area <= areaPerBucket;
    }
    public static void main(String[] args) {
        System.out.println(canPack(-3.4, 2.1, 1.5, 2));
        System.out.println(canPack(3.4, 2.1, 1.5, 2));
        System.out.println(canPack(2.75, 3.25, 2.5, 1));

        System.out.println(canPack(-3.4, 2.1, 1.5));
        System.out.println(canPack(3.4, 2.1, 1.5));
        System.out.println(canPack(7.25, 4.3, 2.35));

        System.out.println(canPack(3.4, 1.5));
        System.out.println(canPack(6.26, 2.2));
        System.out.println(canPack(3.26, 0.75));
    }
}