import java.io.*;

public class zad1 {

    public static void main(String[] args) {
        int number1 = 13;
        int number2 = -8;
        int number3 = 1997;

        checkNumber(number1);
        checkNumber(number2);
        checkNumber(number3);
    }

    public static void checkNumber(int number) {
        if (number > 0) {
            System.out.println("positive");
        } else if (number < 0) {
            System.out.println("negative");
        } else {
            System.out.println("zero");
        }
    }
}
