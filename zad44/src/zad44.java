import java.util.ArrayList;
public class zad44 {
    private String phoneNumber;
    private ArrayList<Person> contactList;

    public void zad44(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        this.contactList = new ArrayList<Person>();
    }

    public boolean addNewContact(Person person) {
        if (findContact(person.getName()) >= 0) {
            System.out.println("Contact already exists.");
            return false;
        }

        contactList.add(person);
        return true;
    }

    public boolean updateContact(Person oldPerson, Person newPerson) {
        int position = findContact(oldPerson);
        if (position < 0) {
            System.out.println("Contact not found.");
            return false;
        }

        contactList.set(position, newPerson);
        System.out.println(oldPerson.getName() + " was updated to " + newPerson.getName());
        return true;
    }

    public boolean removeContact(Person person) {
        int position = findContact(person);
        if (position < 0) {
            System.out.println("Contact not found.");
            return false;
        }

        contactList.remove(position);
        System.out.println(person.getName() + " was removed.");
        return true;
    }

    private int findContact(Person person) {
        return contactList.indexOf(person);
    }

    private int findContact(String name) {
        for (int i = 0; i < contactList.size(); i++) {
            Person person = contactList.get(i);
            if (person.getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public Person queryContact(String name) {
        int position = findContact(name);
        if (position >= 0) {
            return contactList.get(position);
        }
        return null;
    }

    public void printContacts() {
        System.out.println("Contact List:");
        for (int i = 0; i < contactList.size(); i++) {
            Person person = contactList.get(i);
            System.out.println((i + 1) + ". " + person.getName() + " -> " + person.getPhoneNumber());
        }
    }

}