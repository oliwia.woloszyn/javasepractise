public class zad10 {

    public static void printYearsAndDays(long min) {
        if (min < 0) {
            System.out.println("Invalid Value");
        } else {
            long hoursinday = 24;
            long daysinyear = 365;
            long minutesinhour = 60;

            long years = min / (daysinyear * hoursinday * minutesinhour);
            long remainingMinutes = min % (daysinyear * hoursinday * minutesinhour);
            long days = (int) remainingMinutes / (hoursinday * minutesinhour);

            System.out.println(min + " min = " + years + " year and " + days + " day");
        }
    }

    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
        printYearsAndDays(-100);
    }
}





