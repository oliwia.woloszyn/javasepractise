import java.util.Scanner;
public class zad42 {
    public static void main(String[] args) {
        int n = getNumberOfElements();
        int[] data = getInputData(n);
        int minimum = findMinimumValue(data);
        displayMinimumValue(minimum);
    }
    public static int getNumberOfElements() {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Enter the number of data elements: ");
        return inputScanner.nextInt();
    }
    public static int[] getInputData(int n) {
        Scanner inputScanner = new Scanner(System.in);
        int[] data = new int[n];
        System.out.println("Enter " + n + " data elements:");
        for (int i = 0; i < n; i++) {
            data[i] = inputScanner.nextInt();
        }
        return data;
    }
    public static int findMinimumValue(int[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException("The data array is empty.");
        }

        int min = data[0];
        for (int i = 1; i < data.length; i++) {
            if (data[i] < min) {
                min = data[i];
            }
        }
        return min;
    }
    public static void displayMinimumValue(int minimum) {
        System.out.println("The minimum value in the data is: " + minimum);
    }
}