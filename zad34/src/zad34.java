public class zad34 {
    private double realPart;
    private double imaginaryPart;

    public zad34(double realPart, double imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public double getRealPart() {
        return realPart;
    }

    public double getImaginaryPart() {
        return imaginaryPart;
    }

    public void addToRealAndImaginary(double real, double imaginary) {
        this.realPart += real;
        this.imaginaryPart += imaginary;
    }

    public void addToComplexNum(zad34 complexNum) {
        this.realPart += complexNum.getRealPart();
        this.imaginaryPart += complexNum.getImaginaryPart();
    }

    public void subtractFromRealAndImaginary(double real, double imaginary) {
        this.realPart -= real;
        this.imaginaryPart -= imaginary;
    }

    public void subtractFromComplexNum(zad34 complexNum) {
        this.realPart -= complexNum.getRealPart();
        this.imaginaryPart -= complexNum.getImaginaryPart();
    }

    public static void main(String[] args) {
        zad34 num1 = new zad34(1.0, 1.0);
        zad34 num2 = new zad34(2.5, -1.5);

        num1.addToRealAndImaginary(1, 1);
        System.out.println("num1.real= " + num1.getRealPart());
        System.out.println("num1.imaginary= " + num1.getImaginaryPart());

        num1.subtractFromComplexNum(num2);
        System.out.println("num1.real= " + num1.getRealPart());
        System.out.println("num1.imaginary= " + num1.getImaginaryPart());

        num2.subtractFromComplexNum(num1);
        System.out.println("num2.real= " + num2.getRealPart());
        System.out.println("num2.imaginary= " + num2.getImaginaryPart());
    }
}