public class zad24 {


        public static void numberToWords(int number) {
            if (number < 0) {
                System.out.println("Invalid Value");
            } else if (number == 0) {
                System.out.println("Zero");
            } else {
                int reversedNumber = reverse(number);
                int originalDigitCount = getDigitCount(number);
                int reversedDigitCount = getDigitCount(reversedNumber);

                while (reversedNumber > 0) {
                    int digit = reversedNumber % 10;
                    reversedNumber /= 10;

                    switch (digit) {
                        case 0 -> System.out.print("Zero ");
                        case 1 ->  System.out.print("One ");
                        case 2 -> System.out.print("Two ");
                        case 3 -> System.out.print("Three ");
                        case 4 -> System.out.print("Four ");
                        case 5 -> System.out.print("Five ");
                        case 6 -> System.out.print("Six ");
                        case 7 -> System.out.print("Seven ");
                        case 8 -> System.out.print("Eight ");
                        case 9 -> System.out.print("Nine ");
                    }
                }

                while (originalDigitCount > reversedDigitCount) {
                    System.out.print("Zero ");
                    reversedDigitCount++;
                }
                System.out.println();

                if (isPalindrome(number)) {
                    System.out.println("The number is a palindrome.");
                } else {
                    System.out.println("The number is not a palindrome.");
                }
            }
        }

        public static int reverse(int number) {
            int reversedNumber = 0;
            int tempNumber = Math.abs(number);

            while (tempNumber > 0) {
                int lastDigit = tempNumber % 10;
                reversedNumber = reversedNumber * 10 + lastDigit;
                tempNumber /= 10;
            }

            return number < 0 ? -reversedNumber : reversedNumber;
        }

        public static int getDigitCount(int number) {
            if (number < 0) {
                return -1;
            }

            if (number == 0) {
                return 1;
            }

            int count = 0;
            while (number > 0) {
                count++;
                number /= 10;
            }

            return count;
        }

        public static boolean isPalindrome(int number) {
            return number == reverse(number);
        }
    public static void main(String[] args) {
        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);
    }
    }