import java.util.ArrayList;
import java.util.List;


interface ISaveable  {
    List<String> write();
    void read(List<String> savedValues);
}

public class zad48 implements ISaveable{
    private String name;
    private int hitPoints;
    private int strength;
    private String weapon;

    public zad48(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
        this.weapon = "Sword";
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }


    @Override
    public List<String> write() {
        List<String> values = new ArrayList<>();
        values.add(name);
        values.add(Integer.toString(hitPoints));
        values.add(Integer.toString(strength));
        values.add(weapon);
        return values;
    }


    @Override
    public void read(List<String> savedValues) {
        if (savedValues != null && savedValues.size() == 4) {
            name = savedValues.get(0);
            hitPoints = Integer.parseInt(savedValues.get(1));
            strength = Integer.parseInt(savedValues.get(2));
            weapon = savedValues.get(3);
        }
    }


    @Override
    public String toString() {
        return "Player{name='" + name + "', hitPoints=" + hitPoints + ", strength=" + strength + ", weapon='" + weapon + "'}";
    }
}


class Monster implements ISaveable {
    private String name;
    private int hitPoints;
    private int strength;

    public Monster(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }


    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }


    @Override
    public List<String> write() {
        List<String> values = new ArrayList<>();
        values.add(name);
        values.add(Integer.toString(hitPoints));
        values.add(Integer.toString(strength));
        return values;
    }


    @Override
    public void read(List<String> savedValues) {
        if (savedValues != null && savedValues.size() == 3) {
            name = savedValues.get(0);
            hitPoints = Integer.parseInt(savedValues.get(1));
            strength = Integer.parseInt(savedValues.get(2));
        }
    }


    @Override
    public String toString() {
        return "Monster{name='" + name + "', hitPoints=" + hitPoints + ", strength=" + strength + "}";
    }
    public static class Main {
        public static void main(String[] args) {
            zad48 player = new zad48("Player 1", 100, 10);
            Monster monster = new Monster("Monster", 50, 5);


            List<String> playerData = player.write();
            List<String> monsterData = monster.write();


            System.out.println("Player data: " + playerData);
            System.out.println("Monster data:: " + monsterData);


            zad48 newPlayer = new zad48("", 0, 0);
            Monster newMonster = new Monster("", 0, 0);

            newPlayer.read(playerData);
            newMonster.read(monsterData);


            System.out.println("Read player data:: " + newPlayer);
            System.out.println("Read monster data:: " + newMonster);
        }
    }
}