public class zad32 {

        private int xCoordinate;
        private int yCoordinate;

        public zad32() {
            this.xCoordinate = 0;
            this.yCoordinate = 0;
        }

        public zad32(int x, int y) {
            this.xCoordinate = x;
            this.yCoordinate = y;
        }

        public int getXCoordinate() {
            return xCoordinate;
        }

        public int getYCoordinate() {
            return yCoordinate;
        }

        public void setXCoordinate(int x) {
            this.xCoordinate = x;
        }

        public void setYCoordinate(int y) {
            this.yCoordinate = y;
        }

        public double calculateDistance() {
            return Math.sqrt(xCoordinate * xCoordinate + yCoordinate * yCoordinate);
        }

        public double calculateDistance(int x, int y) {
            return Math.sqrt((x - xCoordinate) * (x - xCoordinate) + (y - yCoordinate) * (y - yCoordinate));
        }

        public double calculateDistance(zad32 another) {
            return Math.sqrt((another.xCoordinate - xCoordinate) * (another.xCoordinate - xCoordinate) +
                    (another.yCoordinate - yCoordinate) * (another.yCoordinate - yCoordinate));
        }

        public static void main(String[] args) {
            zad32 firstPoint = new zad32(6, 5);
            zad32 secondPoint = new zad32(3, 1);
            System.out.println("Distance  to firstPoint: " + firstPoint.calculateDistance());
            System.out.println("Distance firstPoint/secondPoint: " + firstPoint.calculateDistance(secondPoint));
            System.out.println("Distance from (2,2) to firstPoint: " + firstPoint.calculateDistance(2, 2));
            zad32 origin = new zad32();
            System.out.println("Distance from (0,0) to origin: " + origin.calculateDistance());
        }
    }
