public class zad17 {


    public static int sumFirstAndLastDigit(int number) {
        int lastDigit = getLastDigit(number);
        int firstDigit = getFirstDigit(number);

        if (firstDigit == -1 || lastDigit == -1) {
            return -1;
        }

        return firstDigit + lastDigit;
    }

    public static int getLastDigit(int number) {
        int lastDigit = number % 10;
        return lastDigit;
    }

    public static int getFirstDigit(int number) {
        if (number < 0) {
            return -1;
        }

        while (number >= 10) {
            number /= 10;
        }

        return number;
    }
    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(252));
        System.out.println(sumFirstAndLastDigit(257));
        System.out.println(sumFirstAndLastDigit(0));
        System.out.println(sumFirstAndLastDigit(5));
        System.out.println(sumFirstAndLastDigit(-10));
    }
}