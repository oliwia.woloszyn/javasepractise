public class RCC {
    private zad33 roomFloor;
    private CC carpetCost;

    public RCC(zad33 floor, CC carpet) {
        this.roomFloor = floor;
        this.carpetCost = carpet;
    }

    public double calculateTotalCost() {
        return roomFloor.calculateArea() * carpetCost.getUnitPrice();
    }
}
