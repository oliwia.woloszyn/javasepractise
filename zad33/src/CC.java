public class CC {
    private double unitPrice;

    public CC(double price) {
        if (price < 0) {
            this.unitPrice = 0;
        } else {
            this.unitPrice = price;
        }
    }

    public double getUnitPrice() {
        return unitPrice;
    }
}
