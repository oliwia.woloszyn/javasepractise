public class zad33 {

        private double roomWidth;
        private double roomLength;

        public zad33(double width, double length) {
            if (width < 0) {
                this.roomWidth = 0;
            } else {
                this.roomWidth = width;
            }

            if (length < 0) {
                this.roomLength = 0;
            } else {
                this.roomLength = length;
            }
        }

        public double calculateArea() {
            return roomWidth * roomLength;
        }
    }

