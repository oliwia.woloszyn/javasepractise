public class Main {
    public static void main(String[] args) {
        zad40 hamburger = new zad40("Basic", "Sausage", 3.56, "White");
        hamburger.addAddition(new Addition("Tomato", 0.27));
        hamburger.addAddition(new Addition("Lettuce", 0.75));
        hamburger.addAddition(new Addition("Cheese", 1.13));
        System.out.println("Total Burger price is " + hamburger.calculatePrice());

        HealthyBurger healthyBurger = new HealthyBurger("Bacon", 5.67);
        healthyBurger.addAddition(new Addition("Egg", 5.43));
        healthyBurger.addAddition(new Addition("Lentils", 3.41));
        System.out.println("Total Healthy Burger price is " + healthyBurger.calculatePrice());

        DeluxeBurger db = new DeluxeBurger();
        db.addAddition(new Addition("Should not do this", 50.53));
        System.out.println("Total Deluxe Burger price is " + db.calculatePrice());
    }
}
