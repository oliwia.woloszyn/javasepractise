public class zad40 {

        private String name;
        private String meat;
        private double basePrice;
        private String breadRollType;

        private Addition addition1;
        private Addition addition2;
        private Addition addition3;
        private Addition addition4;

        public zad40(String name, String meat, double basePrice, String breadRollType) {
            this.name = name;
            this.meat = meat;
            this.basePrice = basePrice;
            this.breadRollType = breadRollType;
        }

        public void addAddition(Addition addition) {
            if (addition1 == null) {
                addition1 = addition;
            } else if (addition2 == null) {
                addition2 = addition;
            } else if (addition3 == null) {
                addition3 = addition;
            } else if (addition4 == null) {
                addition4 = addition;
            } else {
                System.out.println("Cannot add more than 4 additions to a hamburger");
            }
        }

        public double calculatePrice() {
            double totalPrice = basePrice;
            System.out.println(name + " hamburger on a " + breadRollType + " roll with " + meat + ", price is " + basePrice);

            if (addition1 != null) {
                totalPrice += addition1.getPrice();
                System.out.println("Added " + addition1.getName() + " for an extra " + addition1.getPrice());
            }

            if (addition2 != null) {
                totalPrice += addition2.getPrice();
                System.out.println("Added " + addition2.getName() + " for an extra " + addition2.getPrice());
            }

            if (addition3 != null) {
                totalPrice += addition3.getPrice();
                System.out.println("Added " + addition3.getName() + " for an extra " + addition3.getPrice());
            }

            if (addition4 != null) {
                totalPrice += addition4.getPrice();
                System.out.println("Added " + addition4.getName() + " for an extra " + addition4.getPrice());
            }

            return totalPrice;
        }
    }

    class Addition {
        private String name;
        private double price;

        public Addition(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }
    }

    class DeluxeBurger extends zad40 {
        public DeluxeBurger() {
            super("Deluxe", "Sausage & Bacon", 14.54, "White");
            addAddition(new Addition("Chips", 2.75));
            addAddition(new Addition("Drink", 1.81));
        }

        @Override
        public void addAddition(Addition addition) {
            System.out.println("Cannot add additional items to a deluxe burger");
        }
    }

    class HealthyBurger extends zad40 {
        public HealthyBurger(String meat, double basePrice) {
            super("Healthy", meat, basePrice, "Brown rye");
        }
    }

