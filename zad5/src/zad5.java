import java.io.IOException;

public class zad5 {

    public static void main(String[] args) throws IOException {
        System.out.println(Year(2000));
        System.out.println(Year(-1600));
        System.out.println(Year(2017));
        System.out.println(Year(1600));


    }
    public static boolean Year(int year) {
        if (year < 1 || year > 9999) {
            return false;
        }
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
}