public class zad19 {

    public static boolean hasSharedDigit(int num1, int num2) {
        if (isInRange(num1) && isInRange(num2)) {
            int[] num1Digits = getDigits(num1);
            int[] num2Digits = getDigits(num2);

            for (int digit1 : num1Digits) {
                for (int digit2 : num2Digits) {
                    if (digit1 == digit2) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    public static boolean isInRange(int number) {
        return number >= 10 && number <= 99;
    }
    public static int[] getDigits(int number) {
        int[] digits = new int[2];
        digits[0] = number % 10;
        digits[1] = number / 10;
        return digits;
    }
    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23));
        System.out.println(hasSharedDigit(9, 99));
        System.out.println(hasSharedDigit(15, 55));
    }

}