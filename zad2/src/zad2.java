public class zad2 {

    public static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour < 0) {
            return -1;
        } else {
            return Math.round(kilometersPerHour * 0.62137119);
        }
    }

    public static void printSpeedConversion(double kilometersPerHour) {
        if (kilometersPerHour < 0) {
            System.out.println("Invalid Value");
        } else {
            long milesPerHour = toMilesPerHour(kilometersPerHour);
            System.out.println(kilometersPerHour + " km/h = " + milesPerHour + " mi/h");
        }
    }
    public static void main(String[] args) {
        printSpeedConversion(1.5);
        printSpeedConversion(10.25);
        printSpeedConversion(-5.6);
        printSpeedConversion(25.42);
        printSpeedConversion(75.114);
    }
}