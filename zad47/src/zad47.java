
public class zad47<ListItem> {
    interface CustomList {
        ListItem getHead();
        boolean addItem(ListItem item);
        boolean deleteItem(ListItem item);
        void traverseList(ListItem head);

        boolean deleteItem(CustomItem item);

        void traverseList(CustomItem head);
    }


    abstract static class CustomItem {
        protected CustomItem rightLink = null;
        protected CustomItem leftLink = null;
        protected Object data;

        public CustomItem(Object data) {
            this.data = data;
        }

        abstract CustomItem next();
        abstract CustomItem setNext(CustomItem item);
        abstract CustomItem previous();
        abstract CustomItem setPrevious(CustomItem item);
        abstract int compareTo(CustomItem item);

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }
    }


    class CustomNode extends CustomItem {
        public CustomNode(Object data) {
            super(data);
        }

        @Override
        CustomItem next() {
            return this.rightLink;
        }

        @Override
        CustomItem setNext(CustomItem item) {
            this.rightLink = item;
            return rightLink;
        }

        @Override
        CustomItem previous() {
            return this.leftLink;
        }

        @Override
        CustomItem setPrevious(CustomItem item) {
            this.leftLink = item;
            return leftLink;
        }

        @Override
        int compareTo(CustomItem item) {
            if (item != null) {
                return ((String) super.getData()).compareTo((String) item.getData());
            } else {
                return -1;
            }
        }
    }


    class CustomLinkedList implements CustomList {
        private CustomItem head = null;

        public CustomLinkedList(CustomItem head) {
            this.head = head;
        }

        @Override
        public ListItem getHead() {
            return (ListItem) head;
        }

        @Override
        public boolean addItem(ListItem item) {
            return false;
        }

        @Override
        public boolean deleteItem(ListItem item) {
            return false;
        }

        @Override
        public void traverseList(ListItem head) {

        }

        @Override
        public boolean deleteItem(CustomItem item) {
            if (head == null || item == null) {
                return false;
            }

            CustomItem currentItem = head;
            CustomItem parentItem = null;

            while (currentItem != null) {
                int comparison = currentItem.compareTo(item);
                if (comparison == 0) {
                    performDeletion(item, parentItem);
                    return true;
                } else if (comparison < 0) {
                    parentItem = currentItem;
                    currentItem = currentItem.next();
                } else {
                    parentItem = currentItem;
                    currentItem = currentItem.previous();
                }
            }

            return false;
        }

        private void performDeletion(CustomItem itemToDelete, CustomItem parentItem) {
            if (itemToDelete.next() == null && itemToDelete.previous() == null) {
                if (parentItem != null) {
                    int comparison = parentItem.compareTo(itemToDelete);
                    if (comparison < 0) {
                        parentItem.setNext(null);
                    } else {
                        parentItem.setPrevious(null);
                    }
                } else {
                    head = null;
                }
            } else if (itemToDelete.next() == null) {
                if (parentItem != null) {
                    int comparison = parentItem.compareTo(itemToDelete);
                    if (comparison < 0) {
                        parentItem.setNext(itemToDelete.previous());
                    } else {
                        parentItem.setPrevious(itemToDelete.previous());
                    }
                } else {
                    head = itemToDelete.previous();
                }
            } else if (itemToDelete.previous() == null) {
                if (parentItem != null) {
                    int comparison = parentItem.compareTo(itemToDelete);
                    if (comparison < 0) {
                        parentItem.setNext(itemToDelete.next());
                    } else {
                        parentItem.setPrevious(itemToDelete.next());
                    }
                } else {
                    head = itemToDelete.next();
                }
            } else {
                CustomItem current = itemToDelete.next();
                CustomItem currentParent = itemToDelete;
                while (current.previous() != null) {
                    currentParent = current;
                    current = current.previous();
                }
                itemToDelete.setData(current.getData());
                if (currentParent == itemToDelete) {
                    currentParent.setNext(current.next());
                } else {
                    currentParent.setPrevious(current.next());
                }
            }
        }

        @Override
        public void traverseList(CustomItem head) {
            if (head == null) {
                System.out.println("The list is empty.");
            } else {
                if (head.previous() != null) {
                    traverseList(head.previous());
                }
                System.out.println(head.getData());
                if (head.next() != null) {
                    traverseList(head.next());
                }
            }
        }

    }}