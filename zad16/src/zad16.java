public class zad16 {

    public static boolean isPalindrome(int number) {
        if (number < 0) {
            return false;
        } else if (number < 10) {
            return true;
        }

        int originalNumber = number;
        int reversedNumber = 0;

        while (number != 0) {
            int lastDigit = number % 10;
            reversedNumber = reversedNumber * 10 + lastDigit;
            number /= 10;
        }

        if (originalNumber == reversedNumber) {
            return true;
        } else {
            return false;
        }

    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }

}