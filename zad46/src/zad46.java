import java.util.ArrayList;
public class zad46 {
    private String bankName;
    private ArrayList<BranchCustom> branches;

    public zad46(String bankName) {
        this.bankName = bankName;
        this.branches = new ArrayList<>();
    }

    public boolean addBranch(String branchName) {
        if (findBranch(branchName) == null) {
            branches.add(new BranchCustom(branchName));
            return true;
        }
        return false;
    }

    public boolean addCustomer(String branchName, String customerName, double initialTransaction) {
        BranchCustom branch = findBranch(branchName);
        if (branch != null) {
            return branch.newCustomer(customerName, initialTransaction);
        }
        return false;
    }

    public boolean addCustomerTransaction(String branchName, String customerName, double transaction) {
        BranchCustom branch = findBranch(branchName);
        if (branch != null) {
            return branch.addCustomerTransaction(customerName, transaction);
        }
        return false;
    }

    private BranchCustom findBranch(String branchName) {
        for (BranchCustom branch : branches) {
            if (branch.getBranchName().equals(branchName)) {
                return branch;
            }
        }
        return null;
    }

    public boolean listCustomers(String branchName, boolean showTransactions) {
        BranchCustom branch = findBranch(branchName);
        if (branch != null) {
            ArrayList<CustomerCustom> customers = branch.getCustomers();
            System.out.println("Customer details for branch " + branchName);
            for (int i = 0; i < customers.size(); i++) {
                CustomerCustom customer = customers.get(i);
                System.out.println("Customer: " + customer.getCustomerName() + "[" + (i + 1) + "]");
                if (showTransactions) {
                    ArrayList<Double> transactions = customer.getTransactions();
                    System.out.println("Transactions:");
                    for (int j = 0; j < transactions.size(); j++) {
                        System.out.println("[" + (j + 1) + "] Amount " + transactions.get(j));
                    }
                }
            }
            return true;
        }
        return false;
    }
}

class BranchCustom {
    private String branchName;
    private ArrayList<CustomerCustom> customers;

    public BranchCustom(String branchName) {
        this.branchName = branchName;
        this.customers = new ArrayList<>();
    }

    public String getBranchName() {
        return branchName;
    }

    public ArrayList<CustomerCustom> getCustomers() {
        return customers;
    }

    public boolean newCustomer(String customerName, double initialTransaction) {
        if (findCustomer(customerName) == null) {
            customers.add(new CustomerCustom(customerName, initialTransaction));
            return true;
        }
        return false;
    }

    public boolean addCustomerTransaction(String customerName, double transaction) {
        CustomerCustom customer = findCustomer(customerName);
        if (customer != null) {
            customer.addTransaction(transaction);
            return true;
        }
        return false;
    }

    private CustomerCustom findCustomer(String customerName) {
        for (CustomerCustom customer : customers) {
            if (customer.getCustomerName().equals(customerName)) {
                return customer;
            }
        }
        return null;
    }
}

class CustomerCustom {
    private String customerName;
    private ArrayList<Double> transactions;

    public CustomerCustom(String customerName, double initialTransaction) {
        this.customerName = customerName;
        this.transactions = new ArrayList<>();
        addTransaction(initialTransaction);
    }

    public String getCustomerName() {
        return customerName;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }

    public void addTransaction(double transaction) {
        transactions.add(transaction);
    }
}

