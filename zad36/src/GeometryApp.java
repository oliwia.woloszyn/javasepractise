public class GeometryApp {
    public static void main(String[] args) {
        zad36 rectangle = new zad36(5, 10);
        System.out.println("rectangle.width= " + rectangle.getWidth());
        System.out.println("rectangle.length= " + rectangle.getLength());
        System.out.println("rectangle.area= " + rectangle.calculateArea());

        BoxShape box = new BoxShape(5, 10, 5);
        System.out.println("box.width= " + box.getWidth());
        System.out.println("box.length= " + box.getLength());
        System.out.println("box.area= " + box.calculateArea());
        System.out.println("box.depth= " + box.getDepth());
        System.out.println("box.volume= " + box.calculateVolume());
    }
}
