public class zad36 {
    private double width;
    private double length;

    public zad36(double width, double length) {
        this.width = (width < 0) ? 0 : width;
        this.length = (length < 0) ? 0 : length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double calculateArea() {
        return width * length;
    }
}

class BoxShape extends zad36 {
    private double depth;

    public BoxShape(double width, double length, double depth) {
        super(width, length);
        this.depth = (depth < 0) ? 0 : depth;
    }

    public double getDepth() {
        return depth;
    }

    public double calculateVolume() {
        return calculateArea() * depth;
    }
}

