public class zad43 {
    public static void main(String[] args) {
        int[] arrayToReverse = {11, 22, 33, 44, 55};
        System.out.println("Original Array:");
        displayIntArray(arrayToReverse);

        reverseArrayInPlace(arrayToReverse);

        System.out.println("\nReversed Array:");
        displayIntArray(arrayToReverse);
    }

    public static void reverseArrayInPlace(int[] arr) {
        int length = arr.length;
        for (int i = 0; i < length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[length - 1 - i];
            arr[length - 1 - i] = temp;
        }
    }

    public static void displayIntArray(int[] arr) {
        System.out.print("Array Elements: [");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}