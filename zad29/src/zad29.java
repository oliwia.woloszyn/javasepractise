public class zad29 {
    public static void main(String[] args) {
        SimpleCalculator calc = new SimpleCalculator();
        calc.setFirstNumber(5.0);
        calc.setSecondNumber(4);
        System.out.println("Addition result: " + calc.getAdditionResult());
        System.out.println("Subtraction result: " + calc.getSubtractionResult());
        calc.setFirstNumber(5.25);
        calc.setSecondNumber(0);
        System.out.println("Multiplication result: " + calc.getMultiplicationResult());
        System.out.println("Division result: " + calc.getDivisionResult());
    }

}

class SimpleCalculator {
    private double firstNumber;
    private double secondNumber;

    public double getFirstNumber() {
        return firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setFirstNumber(double firstNumber) {
        if (firstNumber < 0) {
            this.firstNumber = 0;
        } else {
            this.firstNumber = firstNumber;
        }
    }

    public void setSecondNumber(double secondNumber) {
        if (secondNumber < 0) {
            this.secondNumber = 0;
        } else {
            this.secondNumber = secondNumber;
        }
    }

    public double getAdditionResult() {
        return firstNumber + secondNumber;
    }

    public double getSubtractionResult() {
        return firstNumber - secondNumber;
    }

    public double getMultiplicationResult() {
        return firstNumber * secondNumber;
    }

    public double getDivisionResult() {
        if (secondNumber == 0) {
            return 0;
        }
        return firstNumber / secondNumber;
    }

}